var querystring = require('querystring');
var http = require('http');
var fs = require('fs');


vals = '1234567890zxcvbnmasdfghjklqwertyuiopZXCVBNMASDFGHJKLQWERTYUIOP';
var random_val = function() {
		var ret = '',
			len = 5 + Math.floor(Math.random() * 32);
		for (var i = 0; i < len; i++)
		ret += vals[Math.round(Math.random() * vals.length)];
		return ret;
	}
var post_data = '';
while (post_data.length < (512 * 1024)) {
	post_data += random_val() + '&' + random_val() + '=';
}


		var start = (+new Date());
		// An object of options to indicate where to post to
		var post_options = {
			host: 'localhost',
			port: '80',
			path: '/',
			method: 'POST',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': post_data.length
			}
		};
		// Set up the request
		var post_req = http.request(post_options, function(res) {
			var len = 0;
			res.setEncoding('utf8');
            
			res.on('data', function(chunk) {
				len += chunk.length;
			});
            
			res.on('end', function() {
                console.log('end')
			});
            
			res.on('error', console.log);
		}).on('error', console.error);
		// post the data
		var pos = 0;
		var interval = setInterval(function() {
			post_req.write(post_data.substr(pos, pos + 2));
			pos += 2;
			if (pos >= post_data.length) {
				clearInterval(interval);
				post_req.end();
			}
		}, 50);