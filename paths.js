var randomString = require('./random-string')

function randomNumber() {
    return '8 (9' +
        Math.random().toFixed(1)[2] +
        Math.random().toFixed(1)[2] +
        ') ' +
        Math.random().toFixed(1)[2] +
        Math.random().toFixed(1)[2] +
        Math.random().toFixed(1)[2] +
        '-' +
        Math.random().toFixed(1)[2] +
        Math.random().toFixed(1)[2] +
        '-'+
        Math.random().toFixed(1)[2] +
        Math.random().toFixed(1)[2]
}

module.exports = [
    {
        url: 'http://apple-repair.ru/order/',
        method: 'POST',
        fields: () => ({
            phone: randomNumber(),
            submit: '»'
        })
    },
    {
        url: 'http://apple-repair.ru/order/',
        method: 'POST',
        fields: () => ({
            phone: randomNumber(),
            name: randomString( Math.round(Math.random() * 16) + 5 ),
            submit: '»'
        })
    }
]