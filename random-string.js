const vals = '1234567890zxcvbnmasdfghjklqwertyuiopZXCVBNMASDFGHJKLQWERTYUIOP'

function randomString( len ) {
    var ret = '';

    for (var i = 0; i < len; i++) {
        ret += vals[ Math.round(Math.random() * vals.length) ];
    }

    return ret;
}

module.exports = randomString
module.exports.randomString = randomString
