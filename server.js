var http = require('http'),
    _req;

var app = http.createServer( (request, response) => {
    if (request.method == 'POST') {
        var body = '';

        request.pipe(process.stdout)
        request.on('data', function (data) {
//            console.log( 'Receive POST data', data.toString().length )

            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
//            if (body.length > 1e6)
//                request.connection.destroy();
        });

        request.on('end', function () {
            console.log('Ended POST')
        });

        request.connection.on('end', function () {
            console.log('Request connection ENDED')
        });

        request.connection.on('close', function () {
            console.log('Request connection CLOSED')
        });
    }
    
    response.writeHead(200, {'Content-Type': 'text/plain'});
    
    var headers = {}
    
    for( var header of Object.keys(request.headers) ) {
        headers[ header ] = request.headers[ header ]
    }
    
    response.end(JSON.stringify( headers, null, ' ' ));
    
    console.log( 'Responsed', request === _req )
});

app.on('connection', (req) => {
    _req = req
    console.log('Request recieved')
});

app.on('error', (error) => {
    console.error(error)
});

app.on('end', (error) => {
    console.log('Connection closed')
});

app.on('close', (error) => {
    console.log('Connection closed')
});

app.listen( 80 )

console.log( 'Started' )
